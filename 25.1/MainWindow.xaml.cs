﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace _25._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
        }

        private void Sony_Checked(object sender, RoutedEventArgs e)
        {

            TV Sony = new TV("Sony", "KD - 77AG9", 100, 85, @"C:\Users\MVP\source\repos\25.1\25.1\Images\Sony KD-77AG9.jpg");
            Merk.Content = "Merk: " + Sony.Merk;
            Type.Content = "Type: " + Sony.Type;
            Herz.Content = "Herz: " + Sony.Herz;
            Beeldgrote.Content = "Beeldgrote: " + Sony.Beeldgrote;
            BitmapImage Sonypic = new BitmapImage(new Uri(@"C:\Users\MVP\source\repos\25.1\25.1\Images\Sony KD-77AG9.jpg"));
            imagebox.Source = Sonypic;


        }
        private void Samsung_Checked(object sender, RoutedEventArgs e)
        {
            TV Samsung = new TV("Samsung", "QLED 8K 85Q950TS", 132, 25, @"C:\Users\MVP\Downloads\Vanaf Les 25 - images\Samsung QLED 8K 85Q950TS (2020).jpg");
            Merk.Content = "Merk: " + Samsung.Merk;
            Type.Content = "Type: " + Samsung.Type;
            Herz.Content = "Herz: " + Samsung.Herz;
            Beeldgrote.Content = "Beeldgrote: " + Samsung.Beeldgrote;
            BitmapImage Samsungpic = new BitmapImage(new Uri(@"C:\Users\MVP\Downloads\Vanaf Les 25 - images\Samsung QLED 8K 85Q950TS (2020).jpg"));
            imagebox.Source = Samsungpic;



        }



        private void cbPowerOn(object sender, RoutedEventArgs e)
        {
            if (Sony.IsChecked == true)
            {
                TV Sony = new TV("Sony", "KD - 77AG9", 100, 85, @"C:\Users\MVP\source\repos\25.1\25.1\Images\Sony KD-77AG9.jpg");
                Sony.Kanaal = 1;
                txtKanaal.Text = Sony.Kanaal.ToString();
                Sony.Volume = 22;
                txtVolume.Text = Sony.Volume.ToString();
            }
            else if (Samsung.IsChecked == true)
            {
                TV Samsung = new TV("Samsung", "QLED 8K 85Q950TS", 132, 25, @"C:\Users\MVP\Downloads\Vanaf Les 25 - images\Samsung QLED 8K 85Q950TS (2020).jpg");
                Samsung.Kanaal = 4;
                txtKanaal.Text = Samsung.Kanaal.ToString();
                Samsung.Volume = 20;
                txtVolume.Text = Samsung.Volume.ToString();
            }
        }
    }

}
