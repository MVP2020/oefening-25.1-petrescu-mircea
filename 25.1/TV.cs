﻿namespace _25._1
{
    class TV
    {
        private string _afbeelding;
        private int _beeldgrote;
        private int _herz;
        private int _kanaal;
        private string _merk;
        private bool _power;
        private bool _teletext;
        private string _type;
        private int _volume;

        public string Afbeelding { get; set; }
        public int Beeldgrote { get; set; }
        public int Herz { get; set; }
        public int Kanaal { get; set; }
        public string Merk { get; set; }
        public bool Power { get; set; }
        public bool Teletext { get; set; }

        public string Type { get; set; }

        public int Volume { get; set; }


        public TV(string merk, string type, int herz, int beeldgrote, string afbeelding)
        {
            Merk = merk;
            Type = type;
            Herz = herz;
            Beeldgrote = beeldgrote;
            Afbeelding = afbeelding;

        }

        public override string ToString()
        {
            return base.ToString();
        }

    }
}
